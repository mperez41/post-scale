///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// CUSTOM UI
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

// #include <TFT_eSPI.h>
// #include <DisplayManager.h>
// #include <CustomUI.h>

// TFT_eSPI tft = TFT_eSPI();           // Create instance of the library
// DisplayManager displayManager(&tft); // Create DisplayManager instance
// CustomUI customUI(&displayManager);  // Create CustomUI instance

// void setup()
// {
//   displayManager.init(); // Initialize the display
//   customUI.drawUI();     // Draw the initial UI
// }

// void loop()
// {
//   // Example updates (these would normally be replaced with sensor readings)
//   int randomNumber = random(99);
//   customUI.updateWeight(randomNumber);
//   customUI.updateElapsedTime(120);
//   customUI.updateFlowRate(5);

//   delay(100); // Just for demonstration, delay 1 second
// }

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// Menu System
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

// #include <TFT_eSPI.h>
// #include <DisplayManager.h>
// #include <MenuSystem.h>

// TFT_eSPI tft = TFT_eSPI(); // Create a TFT_eSPI object
// DisplayManager displayManager(&tft); // Create a DisplayManager instance
// MenuSystem menuSystem(&displayManager); // Create a MenuSystem instance

// void setup() {
//   displayManager.init(); // Initialize the display
//   displayManager.loadCustomFont("Final");
//   // The initial UI update is done in the MenuSystem constructor
// }

// void loop() {
//   // Here, you'll check the rotary encoder or any other input to switch modes
//   // For example, if a certain condition is met (e.g., a flag is toggled):
//   // menuSystem.switchMode();
//   // Placeholder for delay to simulate the loop doing work
//   menuSystem.switchMode();
//   delay(3000);
// }

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// CHANGE FONTS EXPERIMENTS
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

// #include <TFT_eSPI.h>
// TFT_eSPI tft = TFT_eSPI(); // Create TFT object

// void setup() {
//   Serial.begin(115200); // Start serial for debugging
//   tft.init();
//   tft.fillScreen(TFT_BLACK);
//   tft.setTextSize(3);
//   tft.loadFont("DRKrapkaRound-Regular20"); // Load your custom font
//   tft.setTextFont("DRKrapkaRound-Regular20");
//   tft.setTextColor(TFT_WHITE, TFT_BLACK); // Set text color
//   tft.setCursor(0, 0);
//   tft.println("Hello, Custom Font!"); // Test text
// }

// void loop() {
//   // Nothing here for now
// }

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// LOAD CELL EXAMPLE
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

#include <Arduino.h>
#include "HX711Manager.h"
#include <TFT_eSPI.h>
#include <DisplayManager.h>
#include "RotaryEncoder.h"

TFT_eSPI tft = TFT_eSPI();           // Create TFT object
DisplayManager displayManager(&tft); // Create DisplayManager instance
RotaryEncoder encoder(1, 2, 3);      // CLK, DT, SW pins respectively
// Define the pins
#define LOADCELL_DOUT_PIN 18
#define LOADCELL_SCK_PIN 43

// Initialize the HX711Manager with the pins and calibration factor
HX711Manager loadCell(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN, -2061.0251333100614);

void setup()
{
  Serial.begin(115200);
  loadCell.begin();
  encoder.begin();
  displayManager.init();
  loadCell.tare(); // Zero the scale
  displayManager.clearDisplay();
}

void loop()
{
  encoder.update();
  float weight = loadCell.getWeight();
  float averageWeight = loadCell.getFilteredWeight(weight);

  displayManager.updateText(String(averageWeight, 1), 10, 10, TFT_WHITE, 3);
  if (encoder.isButtonPressed())
  {
    loadCell.tare();
    delay(10); // Simple debounce
  }
  delay(1); // Read the weight every second
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// ENCODER EXAMPLE
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

// #include <Arduino.h>
// #include <Arduino.h>
// #include "RotaryEncoder.h"
// #include <TFT_eSPI.h>
// TFT_eSPI tft = TFT_eSPI(); // Create TFT object

// RotaryEncoder encoder(1, 2, 3); // CLK, DT, SW pins respectively

// void setup()
// {
//   Serial.begin(9600);
//   encoder.begin();
//   tft.init();
//   tft.fillScreen(TFT_BLACK);
//   tft.setTextSize(3);
//   tft.setTextColor(TFT_WHITE, TFT_BLACK); // Set text color
// }

// void loop()
// {
//   encoder.update(); // Must be called at the beginning of loop() to update the encoder state
//   static long lastPosition = -1;
//   long position = encoder.getPosition();
//   if (position != lastPosition)
//   {
//     tft.setTextColor(TFT_WHITE, TFT_BLACK); // Set text color
//     tft.fillRect(85, 160, 100, 30, TFT_BLACK);
//     tft.setCursor(85, 160);
//     tft.println(position);
//     lastPosition = position;
//   }

//   if (encoder.isButtonPressed())
//   {
//     encoder.reset();
//     // Add debouncing if necessary
//     delay(200); // Simple debounce
//   }
// }
