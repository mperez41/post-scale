#ifndef CustomUI_h
#define CustomUI_h

#include "DisplayManager.h"

class CustomUI
{
public:
  CustomUI(DisplayManager *displayManager); // Constructor
  void drawUI();                            // Method to draw the initial UI
  void updateWeight(float weight);          // Method to update the weight value
  void updateElapsedTime(int elapsedTime);  // Method to update the elapsed time value
  void updateFlowRate(int flowRate);        // Method to update the flow rate value

private:
  DisplayManager *_displayManager;      // Pointer to DisplayManager instance
  int _weight, _elapsedTime, _flowRate; // Variables to store the current values
  void drawStaticElements();            // Helper method to draw static parts of the UI
};

#endif