#include "CustomUI.h"

CustomUI::CustomUI(DisplayManager *displayManager) : _displayManager(displayManager) {}

void CustomUI::drawUI()
{
  drawStaticElements(); // Draw static parts of the UI
  updateWeight(_weight);
  updateElapsedTime(_elapsedTime);
  updateFlowRate(_flowRate);
}

void CustomUI::updateWeight(float weight)
{
  _weight = weight;
  _displayManager->updateText(String(weight, 2), 10, 30, TFT_WHITE, 2);
}

void CustomUI::updateElapsedTime(int elapsedTime)
{
  _elapsedTime = elapsedTime;
  _displayManager->updateText(String(elapsedTime) + " sec", 10, 80, TFT_WHITE, 2);
}

void CustomUI::updateFlowRate(int flowRate)
{
  _flowRate = flowRate;
  _displayManager->updateText(String(flowRate) + " ml/sec", 120, 30, TFT_WHITE, 2);
}

void CustomUI::drawStaticElements()
{
  _displayManager->clearDisplay();
  _displayManager->updateText("Weight:", 10, 10, TFT_WHITE, 2);
  _displayManager->updateText("Elapsed Time:", 10, 60, TFT_WHITE, 2);
  _displayManager->updateText("Flow Rate:", 120, 10, TFT_WHITE, 2);
}