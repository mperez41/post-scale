#ifndef MenuSystem_h
#define MenuSystem_h

#include "DisplayManager.h"

enum Mode
{
  BREW,
  RECIPE
}; // Enum to represent the different modes

class MenuSystem
{
public:
  MenuSystem(DisplayManager *displayManager); // Constructor
  void switchMode();                          // Method to switch between modes
  void updateUI();                            // Method to update the UI based on the current mode
  void setCurrentMode(Mode mode);             // Method to set the current mode directly
  Mode getCurrentMode();                      // Method to get the current mode

private:
  DisplayManager *_displayManager; // Pointer to the DisplayManager instance
  Mode _currentMode;               // Variable to keep track of the current mode
};

#endif