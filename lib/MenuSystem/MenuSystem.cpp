#include "MenuSystem.h"

MenuSystem::MenuSystem(DisplayManager* displayManager) : _displayManager(displayManager), _currentMode(BREW) {
  // Initialize the display with the default mode
  updateUI();
}

void MenuSystem::switchMode() {
  // Switch the current mode
  if (_currentMode == BREW) {
    _currentMode = RECIPE;
  } else {
    _currentMode = BREW;
  }
  updateUI(); // Update the display to reflect the new mode
}

void MenuSystem::updateUI() {
    // Clear the display first
    _displayManager->clearDisplay();

    // Check the current mode and display the corresponding UI
    if (_currentMode == BREW) {
        _displayManager->updateTextWithUnderline("BREW", 10, 10, TFT_WHITE, 2); // Emphasize BREW
        _displayManager->updateText("RECIPE", 10, 40, TFT_WHITE, 2); // Normal Recipe
    } else if (_currentMode == RECIPE) {
        _displayManager->updateText("BREW", 10, 10, TFT_WHITE, 2); // Normal Brew
        _displayManager->updateTextWithUnderline("RECIPE", 10, 40, TFT_WHITE, 2); // Emphasize RECIPE
    }
}

void MenuSystem::setCurrentMode(Mode mode) {
  _currentMode = mode;
  updateUI();
}

Mode MenuSystem::getCurrentMode() {
  return _currentMode;
}