#ifndef InputManager_h
#define InputManager_h

#include <Arduino.h>
#include <Encoder.h>
#include <HX711.h>

// pins for the rotary encoders and da switches
#define ENCODER1_A_PIN 2
#define ENCODER1_B_PIN 3
#define ENCODER1_SW_PIN 4

#define ENCODER2_A_PIN 5
#define ENCODER2_B_PIN 6
#define ENCODER2_SW_PIN 7

// pins for the HX711 load cell
#define LOADCELL_DOUT_PIN 21
#define LOADCELL_SCK_PIN 43

class InputManager
{
private:
    Encoder encoder1;
    Encoder encoder2;
    bool encoder1SwitchState;
    bool encoder2SwitchState;
    HX711 loadCell;

    void readEncoderSwitches();

public:
    InputManager();
    void begin();
    void update();
};

#endif