#include "InputManager.h"

InputManager::InputManager() : encoder1(ENCODER1_A_PIN, ENCODER1_B_PIN), encoder2(ENCODER2_A_PIN, ENCODER2_B_PIN)
{
    encoder1SwitchState = false;
    encoder2SwitchState = false;
}

void InputManager::begin()
{
    pinMode(ENCODER1_SW_PIN, INPUT_PULLUP);
    pinMode(ENCODER2_SW_PIN, INPUT_PULLUP);

    loadCell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
    loadCell.set_scale(); // Calibrate at the beginnig
    loadCell.tare();
}

void InputManager::update()
{
    long encoder1Position = encoder1.read();
    long encoder2Position = encoder2.read();

    readEncoderSwitches();

    float loadCellValue = loadCell.get_units(5); // not sure what this does yet, need to research

    Serial.print("Encoder 1: ");
    Serial.print(encoder1Position);
    Serial.print(" Switch: ");
    Serial.print(encoder1SwitchState);
    Serial.print(", Encoder 2: ");
    Serial.print(encoder2Position);
    Serial.print(" Switch: ");
    Serial.print(encoder2SwitchState);
    Serial.print(", Load Cell: ");
    Serial.println(loadCellValue);
}

void InputManager::readEncoderSwitches()
{
    encoder1SwitchState = digitalRead(ENCODER1_SW_PIN) == LOW; // Assuming active low
    encoder2SwitchState = digitalRead(ENCODER2_SW_PIN) == LOW; // Assuming active low
}