#ifndef RotaryEncoder_h
#define RotaryEncoder_h

#include <Arduino.h>

class RotaryEncoder
{
public:
  RotaryEncoder(int pinCLK, int pinDT, int pinSW);
  void begin();
  void update();
  long getPosition();
  bool isButtonPressed();
  void reset();

private:
  int _pinCLK, _pinDT, _pinSW;
  volatile long _position;
  volatile bool _buttonState;
  int _lastCLK;
};

#endif
