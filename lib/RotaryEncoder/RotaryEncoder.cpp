#include "RotaryEncoder.h"

RotaryEncoder::RotaryEncoder(int pinCLK, int pinDT, int pinSW) : _pinCLK(pinCLK), _pinDT(pinDT), _pinSW(pinSW), _position(0), _buttonState(false)
{
}

void RotaryEncoder::begin()
{
  pinMode(_pinCLK, INPUT_PULLUP);
  pinMode(_pinDT, INPUT_PULLUP);
  pinMode(_pinSW, INPUT_PULLUP);
  _lastCLK = digitalRead(_pinCLK);
}

void RotaryEncoder::update()
{
  int clkState = digitalRead(_pinCLK);
  if (clkState != _lastCLK)
  { // Means the knob is rotating
    // if the DT state is different than the CLK state, that means the encoder is rotating clockwise
    if (digitalRead(_pinDT) != clkState)
    {
      _position++;
    }
    else
    {
      _position--;
    }
  }
  _lastCLK = clkState;

  // Button state reading (active LOW)
  _buttonState = (digitalRead(_pinSW) == LOW);
}
void RotaryEncoder::reset()
{
  _position = 0;
}

long RotaryEncoder::getPosition()
{
  return _position;
}

bool RotaryEncoder::isButtonPressed()
{
  return _buttonState;
}
