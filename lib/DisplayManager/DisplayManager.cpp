#include "DisplayManager.h"

DisplayManager::DisplayManager(TFT_eSPI *tft) : _tft(tft) {}

void DisplayManager::init()
{
  _tft->init();
  _tft->fillScreen(TFT_BLACK);              // Clear the screen with a black background
  _tft->setTextColor(TFT_WHITE, TFT_BLACK); // Set default text color
  _tft->setTextSize(3);
  _tft->setRotation(3);
}

void DisplayManager::clearDisplay()
{
  _tft->fillScreen(TFT_BLACK); // Clear the screen with a black background
}

void DisplayManager::loadCustomFont(const char *fontName)
{
  _tft->loadFont(fontName);
}

void DisplayManager::updateText(const String &text, int16_t x, int16_t y, uint16_t color, uint8_t textSize)
{
  _tft->setTextColor(color, TFT_BLACK); // Set the text color with a black background
  _tft->fillRect(text.length() * 21, y, 25, textSize * 9, TFT_BLACK);
  _tft->setTextSize(textSize);
  _tft->setCursor(x, y);
  _tft->print(text);
}

void DisplayManager::updateTextWithUnderline(const String &text, int16_t x, int16_t y, uint16_t color, uint8_t textSize)
{
  _tft->setTextSize(textSize);
  _tft->setTextColor(color, TFT_BLACK); // Assuming a black background
  _tft->setCursor(x, y);
  _tft->print(text);
  // Draw the underline
  _tft->drawLine(x, y + 8 * textSize, x + text.length() * 6 * textSize, y + 8 * textSize, color); // Adjust based on font size
}