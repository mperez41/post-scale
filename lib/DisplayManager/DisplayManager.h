#ifndef DisplayManager_h
#define DisplayManager_h

#include <Arduino.h>
#include <TFT_eSPI.h>

class DisplayManager
{
public:
  DisplayManager(TFT_eSPI *tft);                                                                            // Constructor that takes a pointer to an existing TFT_eSPI instance
  void init();                                                                                              // Initialize the display
  void clearDisplay();                                                                                      // Clear the display sat the begining
  void updateText(const String &text, int16_t x, int16_t y, uint16_t color, uint8_t textSize);              // Update text on the display
  void updateTextWithUnderline(const String &text, int16_t x, int16_t y, uint16_t color, uint8_t textSize); // Update text with underline
  void loadCustomFont(const char *fontName);

private:
  TFT_eSPI *_tft; // Pointer to the TFT_eSPI instance
};

#endif