#include "HX711Manager.h"

HX711Manager::HX711Manager(int doutPin, int sckPin, float calibrationFactor)
{
    this->loadCellDoutPin = doutPin;
    this->loadCellSckPin = sckPin;
    this->calibrationFactor = calibrationFactor;
}

void HX711Manager::begin()
{
    loadCell.begin(loadCellDoutPin, loadCellSckPin);
    loadCell.set_scale(calibrationFactor);
    loadCell.tare();
}

void HX711Manager::tare()
{
    loadCell.tare(); // Reset the scale to 0
}

float HX711Manager::getWeight(int times)
{
    return loadCell.get_units(times);
}

float HX711Manager::getFilteredWeight(float rawWeight)
{
    // Subtract the oldest reading from the total
    total -= readings[readIndex];
    // Read from the sensor
    readings[readIndex] = rawWeight;
    // Add the latest reading to the total
    total += readings[readIndex];
    // Advance to the next position in the array
    readIndex++;
    // If we're at the end of the array wrap around to the beginning
    if (readIndex >= sizeof(readings) / sizeof(readings[0]))
    {
        readIndex = 0;
    }

    // Increment the count of readings until the buffer is full
    if (count < sizeof(readings) / sizeof(readings[0]))
    {
        count++;
    }

    average = total / count;

    if (abs(average) < threshold)
    {
        average = 0;
    }
    return average;
}