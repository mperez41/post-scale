#ifndef HX711Manager_h
#define HX711Manager_h

#include <Arduino.h>
#include "HX711.h"

class HX711Manager
{
private:
  int loadCellDoutPin;
  int loadCellSckPin;
  HX711 loadCell;
  float calibrationFactor; // This needs to be adjusted based on the load cell. Run the Python script to get the calibration factor
  float readings[10];      // Buffer for the last 10 readings
  int readIndex = 0;       // Index of the current reading
  float total = 0;         // Running total of the readings
  int count = 0;           // Count of readings for averaging
  float average;
  float threshold = 0.5; // Adjust this threshold. Not sure whats up

public:
  HX711Manager(int doutPin, int sckPin, float calibrationFactor);
  void begin();
  void tare();
  float getWeight(int times = 1);
  float getFilteredWeight(float rawWeight);
};

#endif