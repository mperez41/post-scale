Hello!

I am making this scale for my coffee shop. The `lib` folder contains a bunch of example handler classes to make `src/main.cpp` as straightforward as possible. This is super rough and am just getting started. You can find the `calibration/python/training.py` file to calibrate each scale using your own load cell (and the values generated from `calibration/loadcell.cpp`). You should create an array of known values (about 10 taken from a good, calibrated scale) and read each of the raw values of your own scale. Use `calibration/loadcell.cpp` to get those values. Use those values in the `python/training.py`. I run a linear regression on both values to find the best calibration factor in `lib/HX711Manager.h`. For now, that calibration is used when you extantiate the class in `src/main.cpp`.

Enjoy
