
import numpy as np
from sklearn.linear_model import LinearRegression

# Sample data: Replace these with your actual data
# Known weights (in grams, for example)
known_weights = np.array([-21270, -167555, -224091, -93995, -48525]).reshape(-1, 1)  # Reshape for scikit-learn
# Corresponding raw HX711 sensor values for the known weights
sensor_values = np.array([10.3, 81.3, 108.7, 45.7, 23.5])

# Perform linear regression
model = LinearRegression()
model.fit(sensor_values.reshape(-1, 1), known_weights)

# The calibration factor is the slope of the line (model.coef_)
calibration_factor = model.coef_[0][0]
intercept = model.intercept_[0]

print(f"Calibration Factor: {calibration_factor}")
print(f"Intercept (Offset): {intercept}")

# Optionally, test the model with a sensor value
test_sensor_value = -21270  # Example sensor value to get the predicted weight
predicted_weight = model.predict([[test_sensor_value]])[0][0]
print(f"Predicted Weight for sensor value {test_sensor_value}: {predicted_weight} grams")