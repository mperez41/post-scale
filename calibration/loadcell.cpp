// if you are lazy like me, just paste this code in src/main.cpp to get calibration values vs compling and uploading this...
#include "HX711.h"

// HX711 circuit wiring. This is using a esp32. Change this to whatever board you want
const int LOADCELL_DOUT_PIN = 18;
const int LOADCELL_SCK_PIN = 43;

HX711 scale;

void setup()
{
  Serial.begin(57600);
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
}

void loop()
{

  if (scale.is_ready())
  {
    scale.set_scale();
    Serial.println("Tare... remove any weights from the scale.");
    delay(5000);
    scale.tare();
    Serial.println("Tare done...");
    Serial.print("Place a known weight on the scale...");
    delay(5000);
    long reading = scale.get_units(10);
    Serial.print("Result: ");
    Serial.println(reading);
  }
  else
  {
    Serial.println("HX711 not found.");
  }
  delay(1000);
}
